import { describe, it, expect } from 'vitest'

import { shallowMount } from '@vue/test-utils'
import GridCell from '../GridCell.vue'

describe('GridCell', () => {
  it('renders properly', () => {
    const wrapper = shallowMount(GridCell, { props: { val: 1, rowNumber: 2,  colNumber:3} })
    expect(wrapper.find('.grid-cell').exists()).toBe(true);
   // expect(wrapper.find('.grid-cell__value').text()).toBe(1);
   expect(wrapper.find('[data-row="2"]')).toBeTruthy();
   expect(wrapper.find('[data-col="3"]')).toBeTruthy();
   expect(wrapper.text()).toContain('1')
  })
  it('emits on click', () => {
    const wrapper = shallowMount(GridCell, { props: { val: 1, rowNumber: 2,  colNumber:3} })
    wrapper.find('.grid-cell').trigger('click')
    wrapper.vm.$nextTick(() => {
      
    expect(wrapper.emitted()).toHaveProperty('cellClick')

    const cellClickEvent = wrapper.emitted('cellClick')
    expect(cellClickEvent).toHaveLength(1)
    })
  })
})
