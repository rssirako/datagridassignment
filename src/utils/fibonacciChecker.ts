/*
* Checks array on batches of 5 members against the first 30 consequitive fib numbers.
Returns the index of the first match member of 5 consequitive fib. numbers or -1 if there is no match 
* */
export default (arr: number[]): number => {
    let firstThirthyFib = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811]
    for (let i = 0; i < arr.length - 5; i++) {
        for (let j = 0; j < firstThirthyFib.length; j++) {
            if (j < firstThirthyFib.length - 5) {
                const result = firstThirthyFib.slice(j, 5 + j).every(val => arr.slice(i, 5 + i).includes(val))
                if (result) {
                    return i;
                }
            }

        }
    }

    return -1;
}