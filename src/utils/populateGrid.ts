/*
* Generates 50x50 2D array with random values between 0-9
* */
export default (rows = 50, cols = 50) => Array.from(
    { length: rows },
    () => Array.from({ length: cols }, () => Math.floor(Math.random() * 10))
  )